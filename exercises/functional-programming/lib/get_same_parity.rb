# frozen_string_literal: true

# BEGIN
class Array
  def filter_by_parity!(number)
    select { |element| element.odd? == number.odd? }
  end
end

def get_same_parity(array)
  array.filter_by_parity!(array[0])
end
# END
