# frozen_string_literal: true

# BEGIN
class String
  def sort_alphabetically
    chars.sort.join
  end
end

def anagramm_filter(string, array)
  array.select { |word| word.sort_alphabetically == string.sort_alphabetically }
end
# END
