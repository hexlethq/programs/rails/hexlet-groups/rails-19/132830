# frozen_string_literal: true

# BEGIN
def count_by_years(array)
  males = array.select { |person| person[:gender] == 'male' }
  males
    .map { |male| Time.new(male[:birthday]).year.to_s }
    .each_with_object({}) do |date, acc|
      acc[date] = 0 unless acc[date]
      acc[date] += 1
    end
end
# END
