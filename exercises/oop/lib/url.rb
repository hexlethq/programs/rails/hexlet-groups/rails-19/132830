# frozen_string_literal: true

require 'uri'
require 'forwardable'

# BEGIN
class Url
  extend Forwardable
  include Comparable

  attr_accessor :url

  def initialize(url)
    @url = URI(url)
  end

  def_delegators :url, :host, :scheme

  def <=>(other)
    url <=> other.url
  end

  def query_params
    url.query.split('&').each_with_object({}) do |param, acc|
      pair = param.split('=')
      acc[pair.first.to_sym] = pair.last
    end
  end

  def query_param(key, default_value = nil)
    query_params[key] || default_value
  end
end
# END
