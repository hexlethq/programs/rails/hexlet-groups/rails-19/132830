# frozen_string_literal: true

require_relative 'test_helper'
require_relative '../lib/stack'

class StackTest < Minitest::Test
  # BEGIN
  def setup
    @stack_entity = Stack.new(['a', 1, 2])
  end

  def test_pop
    assert_equal 2, @stack_entity.pop!
  end

  def test_push
    assert_equal ['a', 1, 2, 3], @stack_entity.push!(3)
  end

  def test_empty
    assert_equal true, Stack.new([]).empty?
    assert_equal false, @stack_entity.empty?
  end

  def test_to_a
    assert_equal ['a', 1, 2], @stack_entity.to_a
  end

  def test_clear
    assert_equal [], @stack_entity.clear!
  end

  def test_size
    assert_equal 3, @stack_entity.size
  end
  # END
end

test_methods = StackTest.new({}).methods.select { |method| method.start_with? 'test_' }
raise 'StackTest has not tests!' if test_methods.empty?
