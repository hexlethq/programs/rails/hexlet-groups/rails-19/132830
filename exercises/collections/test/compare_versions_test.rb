# frozen_string_literal: true

require_relative 'test_helper'
require_relative '../lib/compare_versions'

class CompareVersionsTest < Minitest::Test
  def test_compare_versions
    assert_equal(1, compare_versions('0.2', '0.1'))
    assert_equal(-1, compare_versions('0.1', '0.2'))
    assert_equal(0, compare_versions('4.2', '4.2'))
    assert_equal(-1, compare_versions('0.2', '0.12'))
    assert_equal(-1, compare_versions('3.2', '4.12'))
    assert_equal(1, compare_versions('3.2', '2.12'))
  end
end
