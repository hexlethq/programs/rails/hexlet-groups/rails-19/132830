# frozen_string_literal: true

# BEGIN
def recognize_version(version)
  version.split('.').map(&:to_i)
end

def compare_versions(version1, version2)
  recognize_version(version1) <=> recognize_version(version2)
end

# END
