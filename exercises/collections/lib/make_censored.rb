# frozen_string_literal: true

def make_censored(text, stop_words)
  # BEGIN
  censored_text = text.split.map do |word|
    if stop_words.include?(word)
      '$#%!'
    else
      word
    end
  end

  censored_text.join(' ')
  # END
end
