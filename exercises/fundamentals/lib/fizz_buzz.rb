# frozen_string_literal: true

# BEGIN
def define_string(number)
  fizz = number % 3
  buzz = number % 5
  if fizz.zero? && buzz.zero?
    'FizzBuzz'
  elsif fizz.zero?
    'Fizz'
  elsif buzz.zero?
    'Buzz'
  else
    number.to_s
  end
end

def fizz_buzz(start, stop)
  return '' if stop < start

  result = (start..stop).to_a.map do |number|
    define_string(number)
  end

  result.join(' ')
end

# END{
