# frozen_string_literal: true

# BEGIN
def iter(number, first_n = 0, second_n = 1)
  second_n = 0 if number == 1

  return second_n if number.zero?

  iter(number - 1, second_n, first_n + second_n)
end

def fibonacci(number)
  return if number.negative?

  iter(number)
end
# END
